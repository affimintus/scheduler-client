class ScheduleParser

  WEEK_DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  class << self
    # week_days - ['monday', 'wednesday']
    def next_schedule_for(week_days, run_time)
      # [1, 3, 5]
      week_day_indexes = to_index(week_days)
      if week_day_indexes.include?(Date.today.wday) && Time.zone.now < Time.zone.parse(run_time)
        # schedule
        Time.zone.parse(run_time)
      else
        next_possible_day = next_schedule_day(week_day_indexes)
        advance_days = calculate_advance_days(next_possible_day)
        Time.zone.parse(run_time).advance(days: advance_days)
      end
    end

    def next_schedule_day(week_day_indexes)
      min_arrays = week_day_indexes.select{|i| i <= Date.today.wday}
      max_arrays = week_day_indexes.select{|i| i > Date.today.wday}
      val = if !max_arrays.blank?
        max_arrays.min
      else
        min_arrays.min
      end
    end

    def calculate_advance_days(next_possible_day)
      current_day = Date.today.wday
      if next_possible_day == current_day
        7
      elsif next_possible_day > current_day
        next_possible_day - current_day
      else
        (7 - current_day) + (next_possible_day)
      end
    end

    def week_day_order
        {
          0 => 'Sunday',
          1 => 'Monday',
          2 => 'Tuesday',
          3 => 'Wednesday',
          4 => 'Thursday',
          5 => 'Friday',
          6 => 'Saturday'
        }
    end

    def to_index(week_days)
      indexes = []
      week_days.each do |wday|
        indexes << week_day_order.key(wday)
      end
      indexes
    end
  end

end
