class ChangeStartAndEndDataTypeInTasks < ActiveRecord::Migration
  def up
    change_column :tasks, :start_at, :date
    change_column :tasks, :end_at, :date
  end

  def down
    change_column :tasks, :start_at, :datetime
    change_column :tasks, :end_at, :datetime
  end
end
