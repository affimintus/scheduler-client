class CreateExecutions < ActiveRecord::Migration
  def change
    create_table :executions do |t|
      t.references :task, index: true
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
