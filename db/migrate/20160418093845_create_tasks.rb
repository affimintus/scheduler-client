class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :executable_path
      t.string :command
      t.datetime :start_at
      t.datetime :end_at
      t.string :execution_days
      t.string :execution_server

      t.timestamps
    end
  end
end
