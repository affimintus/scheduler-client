class AddStatusToExecutions < ActiveRecord::Migration
  def change
    add_column :executions, :status, :string
  end
end
