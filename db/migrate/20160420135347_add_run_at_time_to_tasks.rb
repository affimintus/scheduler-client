class AddRunAtTimeToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :run_at_time, :string
  end
end
