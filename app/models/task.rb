class Task < ActiveRecord::Base

  has_many :executions, dependent: :destroy

  after_save :refresh_queue_job
  before_save :join_execution_days
  after_find :split_execution_days
  before_destroy :dequeue_job

  validates_presence_of :name, :executable_path, :command, :start_at, :end_at, :execution_days, :execution_server, :run_at_time

  def enqueue_job
    if next_run_at_time && next_run_at_time <= self.end_at.end_of_day
      Resque.enqueue_at_with_queue(self.execution_server, next_run_at_time, JobProcessor, self.id)
    end
  end

  def execute!
    current_execution = self.executions.create(start_at: DateTime.now)
    `cd #{self.executable_path} && #{self.command}`
    puts 'here'
    current_execution.end_at = DateTime.now
    current_execution.status = $?.exitstatus == 0 ? 'Success' : 'Failure'
    current_execution.save
    enqueue_job
  end

  def join_execution_days
    self.execution_days = self.execution_days.reject{|d| d.blank?}.join('|') if self.execution_days.is_a?(Array)
  end

  def split_execution_days
    self.execution_days = self.execution_days.split('|')
  end

  def next_run_at_time
    self.split_execution_days if self.execution_days.is_a?(String)
    ScheduleParser.next_schedule_for(self.execution_days, self.run_at_time)
  end

  def refresh_queue_job
    dequeue_job
    enqueue_job
  end

  def dequeue_job
    Resque.remove_delayed_selection {|args| args[0] == self.id }
  end

end
