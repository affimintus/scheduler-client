json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :executable_path, :command, :start_at, :end_at, :execution_days, :execution_server
  json.url task_url(task, format: :json)
end
