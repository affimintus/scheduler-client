module ExecutionsHelper

  def calculate_execution_time(execution)
    begin
      time = (execution.end_at - execution.start_at).round
      "#{time} seconds"
    rescue
      "NA"
    end
  end

end
