module TasksHelper

  def available_server_options
    {'Test Server A' => 'test_server_a', 'Test Server B' => 'test_server_b', 'Test Server C' => 'test_server_c'}
  end

  def execution_days_options
    ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  end

end
