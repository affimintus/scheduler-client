require 'rails_helper'

RSpec.describe Task, type: :model do

  describe 'Validations' do
    [:name, :executable_path, :command, :start_at, :end_at, :execution_days, :execution_server, :run_at_time]. each do |attribute|
      it{is_expected.to validate_presence_of(attribute)}
    end
  end

  describe 'Associations' do
    it{is_expected.to have_many(:executions)}
  end

  context 'instance_methods' do
    before do
      @task = FactoryGirl.create(:task)
    end
    describe 'join_execution_days' do
      it 'joins execution days' do
        @task.execution_days = ['Monday', 'Wednesday']
        expect(@task.join_execution_days).to eq('Monday|Wednesday')
      end
    end

    describe 'split execution days' do
      it 'splits and converts execution days into an array' do
        @task.execution_days = 'Friday|Saturday'
        expect(@task.split_execution_days).to eq(['Friday', 'Saturday'])
      end
    end
  end
end
