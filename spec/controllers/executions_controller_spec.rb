require 'spec_helper'

describe ExecutionsController do

  describe "GET 'index'" do
      it "returns http success" do
        get :index
        expect(response).to be_success
      end

      it 'should render index page' do
        get :index
        expect(response).to render_template('index')
      end

      it "assigns all tasks as @executions" do
        get :index
        expect(assigns(:tasks)) == [@executions]
      end
    end
end
