require 'spec_helper'

describe TasksController do
  describe "response of action's for authenticated super user" do
    before do
      @tasks = FactoryGirl.create(:task)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get :index
      expect(response).to be_success
    end

    it 'should render index page' do
      get :index
      expect(response).to render_template('index')
    end

    it "assigns all tasks as @tasks" do
      get :index
      expect(assigns(:tasks)) == [@tasks]
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get :new, format: 'JS'
      expect(response).to be_success
    end

    it 'should render index new' do
      get :new, format: 'JS'
      expect(response).to render_template(:new)
    end

    it 'should asseign the ledger and transaction as new object' do
      get :new, format: 'JS'
      expect(assigns(:task)).to be_a_new(Task)
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      @tasks = FactoryGirl.create(:task)
      get :show, id: @tasks.id

      expect(response).to be_success
    end

    it 'should render index new' do
      @tasks = FactoryGirl.create(:task)
      get :show, id: @tasks.id
      expect(response).to render_template(:show)
    end
  end

  describe "DELETE 'destroy'" do

    before do
      @task = FactoryGirl.create(:task)
    end

    it "returns http success" do
      expect { delete :destroy, {id: @task.id} }.to change(Task, :count).by(-1)
    end

    it "should redirect to geo_zones index page" do
      delete :destroy, {id: @task.id}
      expect(response).to redirect_to(tasks_path)
    end
  end
end

