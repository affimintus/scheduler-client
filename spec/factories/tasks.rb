FactoryGirl.define do
  factory :task do
    name "MyString"
    executable_path "MyString"
    command "MyString"
    start_at "2016-04-18"
    end_at "2016-04-18"
    execution_days "Wednesday|Thursday|Saturday"
    execution_server "test_server_a"
    run_at_time '12:15'
  end
end

